﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ApiHistogramaRest
{
    [DataContract]
    public class RequestData
    {
        [DataMember]
        public string pfecini { get; set; }
        [DataMember]
        public string pfecfin { get; set; }
        [DataMember]
        public string pfecdep { get; set; }
        [DataMember]
        public string pfecret { get; set; }
        [DataMember]
        public string ptipodur { get; set; }
        [DataMember]
        public int pdiasdur { get; set; }
        [DataMember]
        public string porigen { get; set; }
        [DataMember]
        public string pdestino { get; set; }
        [DataMember]
        public string pititipo { get; set; }
        [DataMember]
        public string pclase { get; set; }
        [DataMember]
        public int padts { get; set; }
        [DataMember]
        public int pchs { get; set; }
        [DataMember]
        public int pinfs { get; set; }
    }

    [DataContract]
    public class Histograma
    {
        [DataMember]
        public string Diadep { get; set; }
        [DataMember]
        public decimal Duracion { get; set; }
        [DataMember]
        public string Diaret { get; set; }
        [DataMember]
        public decimal Tarifa { get; set; }
        [DataMember]
        public string Sel_fecdep { get; set; }
        [DataMember]
        public string Sel_fecret { get; set; }
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServiceHistograma" in both code and config file together.
    [ServiceContract]
    public interface IServiceHistograma
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/ApiHistogramaRest/getPrice",
            Method = "POST", 
            RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<Histograma> GetPrice(RequestData data);
        [OperationContract]
        [WebGet(UriTemplate = "/get?pfecini={pfecini}&pfecfin={pfecfin}&pfecdep={pfecdep}&pfecret={pfecret}&ptipodur={ptipodur}&pdiasdur={pdiasdur}&porigen={porigen}&pdestino={pdestino}&pititipo={pititipo}&pclase={pclase}&padts={padts}&pchs={pchs}&pinfs={pinfs}", 
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json, 
        BodyStyle = WebMessageBodyStyle.Bare)]
        List<Histograma> Get(string pfecini, string pfecfin, string pfecdep, string pfecret, 
                             string ptipodur, int pdiasdur, string porigen, string pdestino, 
                             string pititipo, string pclase, int padts, int pchs, int pinfs);
    }
}
