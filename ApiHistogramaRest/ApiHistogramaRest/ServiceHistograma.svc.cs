﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using ApiHistogramaRest.Conexion;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.IO;

using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using System.Globalization;

namespace ApiHistogramaRest
{
    public class ServiceHistograma : IServiceHistograma
    {

        private List<Histograma> repo;

        static T DeserializarJSON<T>(string cadJson)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(cadJson);
        }

        public ServiceHistograma()
        {
            repo = new List<Histograma>();
        }

        public List<Histograma> GetPrice(RequestData data)
        {
            int count = 0;
            try { 
                var objNMOraParam = new NMOracleParameter();
                var strCon = Data.strCnx_WebsOracle;

                using (var objConnection = new OracleConnection(strCon))
                {
                    using (var ora_Command = new OracleCommand())
                    {

                        ora_Command.Connection = objConnection;

                        ora_Command.CommandText = "AIS_CONSULTAR_DATAFILES.SP_PRICERESULT_SEARCH";
                        ora_Command.CommandType = CommandType.StoredProcedure;

                        ora_Command.Parameters.Add("pfecini", OracleDbType.Varchar2).Value = data.pfecini;
                        ora_Command.Parameters.Add("pfecfin", OracleDbType.Varchar2).Value = data.pfecfin;

                        ora_Command.Parameters.Add("pfecdep", OracleDbType.Varchar2).Value = data.pfecdep;
                        ora_Command.Parameters.Add("pfecret", OracleDbType.Varchar2).Value = data.pfecret;

                        ora_Command.Parameters.Add("ptipodur", OracleDbType.Varchar2).Value = data.ptipodur;
                        ora_Command.Parameters.Add("pdiasdur", OracleDbType.Int32).Value = data.pdiasdur;

                        ora_Command.Parameters.Add("porigen", OracleDbType.Varchar2).Value = data.porigen;
                        ora_Command.Parameters.Add("pdestino", OracleDbType.Varchar2).Value = data.pdestino;

                        ora_Command.Parameters.Add("pititipo", OracleDbType.Varchar2).Value = data.pititipo;
                        ora_Command.Parameters.Add("pclase", OracleDbType.Varchar2).Value = data.pclase;
                        ora_Command.Parameters.Add("padts", OracleDbType.Int32).Value = data.padts;
                        ora_Command.Parameters.Add("pchs", OracleDbType.Int32).Value = data.pchs;
                        ora_Command.Parameters.Add("pinfs", OracleDbType.Int32).Value = data.pinfs;

                        //ora_Command.Parameters.Add("pqry", OracleDbType.Varchar2).Direction = ParameterDirection.Output;
                        ora_Command.Parameters.Add("pcursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        objConnection.Open();

                        OracleDataReader objReader = ora_Command.ExecuteReader();

                        decimal saldo = 0;
                        decimal dur = 0;
                        CultureInfo culture = new CultureInfo("en-US");

                        while (objReader.Read())
                        {

                            //for (int i = 0; i < objReader.FieldCount; i++)
                            //{
                            if (objReader["TARIFA"] != DBNull.Value)
                            {
                                saldo = Convert.ToDecimal(objReader["TARIFA"].ToString(), culture);
                            }
                            if (objReader["DURACION"] != DBNull.Value)
                            {
                                dur = Convert.ToDecimal(objReader["DURACION"].ToString(), culture);
                            }

                            repo.Add(new Histograma()
                            {
                                Diadep = objReader["DIADEP"].ToString(),
                                Duracion = dur,
                                Diaret = objReader["DIARET"].ToString(),
                                Tarifa = saldo,
                                Sel_fecret = objReader["SEL_FECRET"].ToString(),
                                Sel_fecdep = objReader["SEL_FECDEP"].ToString()
                            });
                            // }
                        }

                        objConnection.Close();

                    }
                }

            }
            catch (Exception ex)
            {
                throw new FaultException<string>
                        (ex.Message);
            }


            return repo;
        }

        public List<Histograma> Get(string pfecini, string pfecfin, string pfecdep, string pfecret,
                                    string ptipodur, int pdiasdur, string porigen, string pdestino,
                                    string pititipo, string pclase, int padts, int pchs, int pinfs)
        {

            try
            {
                var objNMOraParam = new NMOracleParameter();
                var strCon = Data.strCnx_WebsOracle;

                using (var objConnection = new OracleConnection(strCon))
                {
                    using (var ora_Command = new OracleCommand())
                    {

                        ora_Command.Connection = objConnection;

                        ora_Command.CommandText = "AIS_CONSULTAR_DATAFILES.SP_PRICERESULT_SEARCH";
                        ora_Command.CommandType = CommandType.StoredProcedure;

                        ora_Command.Parameters.Add("pfecini", OracleDbType.Varchar2).Value = pfecini;
                        ora_Command.Parameters.Add("pfecfin", OracleDbType.Varchar2).Value = pfecfin;

                        ora_Command.Parameters.Add("pfecdep", OracleDbType.Varchar2).Value = pfecdep;
                        ora_Command.Parameters.Add("pfecret", OracleDbType.Varchar2).Value = pfecret;

                        ora_Command.Parameters.Add("ptipodur", OracleDbType.Varchar2).Value = ptipodur;
                        ora_Command.Parameters.Add("pdiasdur", OracleDbType.Int32).Value = pdiasdur;

                        ora_Command.Parameters.Add("porigen", OracleDbType.Varchar2).Value = porigen;
                        ora_Command.Parameters.Add("pdestino", OracleDbType.Varchar2).Value = pdestino;

                        ora_Command.Parameters.Add("pititipo", OracleDbType.Varchar2).Value = pititipo;
                        ora_Command.Parameters.Add("pclase", OracleDbType.Varchar2).Value = pclase;
                        ora_Command.Parameters.Add("padts", OracleDbType.Int32).Value = padts;
                        ora_Command.Parameters.Add("pchs", OracleDbType.Int32).Value = pchs;
                        ora_Command.Parameters.Add("pinfs", OracleDbType.Int32).Value = pinfs;

                        ora_Command.Parameters.Add("pcursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                        objConnection.Open();

                        OracleDataReader objReader = ora_Command.ExecuteReader();

                        decimal saldo = 0;
                        decimal dur = 0;
                        CultureInfo culture = new CultureInfo("en-US");

                        while (objReader.Read())
                        {

                            //for (int i = 0; i < objReader.FieldCount; i++)
                            //{
                                if (objReader["TARIFA"] != DBNull.Value)
                                {
                                    saldo = Convert.ToDecimal(objReader["TARIFA"].ToString(), culture);
                                }
                                if (objReader["DURACION"] != DBNull.Value)
                                {
                                    dur = Convert.ToDecimal(objReader["DURACION"].ToString(), culture);
                                }

                                repo.Add(new Histograma()
                                {
                                    Diadep = objReader["DIADEP"].ToString(),
                                    Duracion = dur,
                                    Diaret = objReader["DIARET"].ToString(),
                                    Tarifa = saldo,
                                    Sel_fecret = objReader["SEL_FECRET"].ToString(),
                                    Sel_fecdep = objReader["SEL_FECDEP"].ToString()
                                });
                           // }
                        }

                        objConnection.Close();

                    }
                }

            }
            catch (Exception ex)
            {
                throw new FaultException<string>
                        (ex.Message);
            }


            return repo;
        }
    }
}
