﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;

namespace ApiHistogramaRest.Conexion
{
    public class NMConnection
    {
        public OracleConnection objOracleConexion_Webs = new OracleConnection();

        public void Oracle_WebsConectar()
        {
            objOracleConexion_Webs.ConnectionString = Data.strCnx_WebsOracle;
            objOracleConexion_Webs.Open();
        }
    }
}